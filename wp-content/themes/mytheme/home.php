<?php
/*
Template Name: Home
*/
?>
<?php get_header(); ?>

    <div id="home-content">
        <section id="section1" class="inner section group">
            <div class="col1 col span_5_7">
                <div class="row1 section group">
                    <?php 
                    // Query Arguments
                    $args = array(
                        'posts_per_page' => 5,
                        'category_name' => 'featured',
                    );  
             
                    // The Query
                    $the_query = new WP_Query( $args );

                    if ( $the_query->have_posts() ):
                    ?>
                        <div id="slideshow" class="flexslider col span_2_3">
                            <ul class="slides">
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                    <?php if (has_post_thumbnail()): ?>
                                        <li class="flex-active-slide" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;">
                                            <div class="image">
                                                <?php the_post_thumbnail('thumb-slider'); ?>
                                                <article>
                                                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                                    <?php the_excerpt(); ?>
                                                    <div class="more">
                                                        <span class="date"><?php the_time('F j, Y') ?> /</span>
                                                        <span class="read-more"><a href="<?php the_permalink(); ?>">Read More &raquo;</a></span>
                                                    </div>
                                                </article>                                        
                                            </div>
                                        </li>
                                    <?php endif ?>
                                <?php endwhile; ?>                            
                            </ul>    
                        </div>
                    <?php endif ?>
                    <div id="global" class="col span_1_3">
                        <div id="global-wrapper">
                            <h2><a href="world">Global</a></h2>
                            <?php
                            $args = array(
                                'posts_per_page' => 2,
                                'category_name' => 'world',
                            );  
                     
                            // The Query
                            $the_query = new WP_Query( $args );

                            while ( $the_query->have_posts() ) : $the_query->the_post();
                            ?>                              
                                <article>
                                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                    <?php the_excerpt(); ?>
                                    <div class="more">
                                        <span class="date"><?php the_time('F j, Y') ?> /</span>
                                        <span class="read-more"><a href="<?php the_permalink(); ?>">Read More &raquo;</a></span>
                                    </div>
                                </article>
                            <?php endwhile; ?>      
                        </div>                                      
                    </div>
                </div>
                <div class="row2 section group">
                    <div id="national">
                        <div class="green-line"></div>
                        <div id="slider-controls"><div class="prev"></div><div class="next"></div></div>
                        <h2><a href="national">National</a></h2>
                        <span id="pager">1</span>
                        <?php
                        $args = array(
                            'posts_per_page' => 2,
                            'category_name' => 'national',
                        );  
                 
                        // The Query
                        $the_query = new WP_Query( $args );
                        $the_query2 = new WP_Query( array('category_name' => 'national') );
                        ?>
                        <span id="total-posts"><?php echo $the_query2->found_posts; ?></span>                        
                        <div id="national-article-wrapper">
                            <?
                            while ( $the_query->have_posts() ) : $the_query->the_post();
                            ?>                          
                                <article class="section group">
                                    <?php if (has_post_thumbnail()): ?>
                                        <div class="col span_1_3">
                                        <?php the_post_thumbnail('thumb-national'); ?>
                                        </div>
                                        <aside class="col span_2_3">
                                    <?php else: ?>
                                        <aside class="col">
                                    <?php endif ?>    
                                        <div class="article-info">
                                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                            <?php the_excerpt(); ?>
                                            <div class="more">
                                                <span class="date"><?php the_time('F j, Y') ?> /</span>
                                                <span class="read-more"><a href="<?php the_permalink(); ?>">Read More &raquo;</a></span>
                                            </div>
                                        </div>                      
                                    </aside>
                                </article>
                            <?php endwhile; ?>
                        </div>
                    </div>              
                </div>
            </div>
            <div class="col2 col span_2_7">
                <div id="widget1" class="section group">
                    <?php
                    if (is_active_sidebar('homepage-popular')) :
                        dynamic_sidebar('homepage-popular');
                    endif;
                    ?>
                </div>
                <div id="multimedia" class="section group">
                    <h2><a href="multimedia">Multimedia</a></h2>
                    <?php
                    $args = array(
                        'posts_per_page' => 3,
                        'category_name' => 'multimedia',
                    );  
             
                    // The Query
                    $the_query = new WP_Query( $args );

                    $i = 1;
                    $total = count($the_query);

                    while ( $the_query->have_posts() ) : $the_query->the_post();
                        if ($i==1):
                    ?>
                            <?php if (has_post_thumbnail()): ?>
                                <?php 
                                $video = get_post_custom_values('video'); 
                                if ($video[0] != ''):
                                ?> 
                                    <a href="<?php the_permalink(); ?>"><div id="play-button"></div></a>
                                <?php endif ?>
                                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumb-multimedia'); ?></a>
                            <?php endif ?>
                            <div class="article-wrapper">
                                <article>
                                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                    <?php the_excerpt(); ?>
                                </article>
                            <?php else: ?>
                                <?php if ($i==2): ?>
                                    <div class="article-more">
                                <?php endif ?>
                                        <article>
                                            <h3><div class="arrow-left"></div><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                        </article>
                                <?php if ($i==2 && $total==2 || $i==3): ?>
                                    </div>
                                <?php endif ?>
                            <?php if ($i==$total): ?>
                                </div>
                            <?php endif ?>
                        <?php endif ?>
                    <?php $i++; endwhile; ?>        
                </div>
            </div>
        </section>
        <div class="news inner section group">
            <section class="col1 col span_1_3">
                <div class="section-wrapper">
                    <div class="green-line"></div>
                    <h2><a href="politics">Politics</a></h2>
                    <?php
                    $args = array(
                        'posts_per_page' => 3,
                        'category_name' => 'politics',
                    );  
             
                    // The Query
                    $the_query = new WP_Query( $args );

                    $i = 1;
                    $total = count($the_query);

                    while ( $the_query->have_posts() ) : $the_query->the_post();
                        if ($i==1):
                    ?>                    
                            <article>
                                <?php if (has_post_thumbnail()): ?>
                                    <?php the_post_thumbnail('thumb-category'); ?>
                                <?php endif ?>
                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                <?php the_excerpt(); ?>
                                <div class="more">
                                    <span class="date"><?php the_time('F j, Y') ?> /</span>
                                    <span class="read-more"><a href="<?php the_permalink(); ?>">Read More &raquo;</a></span>
                                </div>
                            </article>
                        <?php else: ?>
                            <?php if ($i==2): ?>
                                <div class="article-more">
                            <?php endif ?>
                                    <article>
                                        <h3><div class="arrow-left"></div><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                    </article>
                            <?php if ($i==2 && $total==2 || $i==3): ?>
                                </div>
                            <?php endif ?>
                        <?php endif ?>
                    <?php $i++; endwhile; ?>  
                </div>                      
            </section>
            <section class="col1 col span_1_3">
                <div class="section-wrapper">
                    <div class="green-line"></div>
                    <h2><a href="business">Business</a></h2>
                    <?php
                    $args = array(
                        'posts_per_page' => 3,
                        'category_name' => 'business',
                    );  
             
                    // The Query
                    $the_query = new WP_Query( $args );

                    $i = 1;
                    $total = count($the_query);

                    while ( $the_query->have_posts() ) : $the_query->the_post();
                        if ($i==1):
                    ?>                    
                            <article>
                                <?php if (has_post_thumbnail()): ?>
                                    <?php the_post_thumbnail('thumb-category'); ?>
                                <?php endif ?>
                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                <?php the_excerpt(); ?>
                                <div class="more">
                                    <span class="date"><?php the_time('F j, Y') ?> /</span>
                                    <span class="read-more"><a href="<?php the_permalink(); ?>">Read More &raquo;</a></span>
                                </div>
                            </article>
                        <?php else: ?>
                            <?php if ($i==2): ?>
                                <div class="article-more">
                            <?php endif ?>
                                    <article>
                                        <h3><div class="arrow-left"></div><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                    </article>
                            <?php if ($i==2 && $total==2 || $i==3): ?>
                                </div>
                            <?php endif ?>
                        <?php endif ?>
                    <?php $i++; endwhile; ?>  
                </div>                              
            </section>
            <section class="col1 col span_1_3">
                <div class="section-wrapper">
                    <div class="green-line"></div>
                    <h2><a href="tech">Tech</a></h2>
                    <?php
                    $args = array(
                        'posts_per_page' => 3,
                        'category_name' => 'tech',
                    );  
             
                    // The Query
                    $the_query = new WP_Query( $args );

                    $i = 1;
                    $total = count($the_query);

                    while ( $the_query->have_posts() ) : $the_query->the_post();
                        if ($i==1):
                    ?>                    
                            <article>
                                <?php if (has_post_thumbnail()): ?>
                                    <?php the_post_thumbnail('thumb-category'); ?>
                                <?php endif ?>
                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                <?php the_excerpt(); ?>
                                <div class="more">
                                    <span class="date"><?php the_time('F j, Y') ?> /</span>
                                    <span class="read-more"><a href="<?php the_permalink(); ?>">Read More &raquo;</a></span>
                                </div>
                            </article>
                        <?php else: ?>
                            <?php if ($i==2): ?>
                                <div class="article-more">
                            <?php endif ?>
                                    <article>
                                        <h3><div class="arrow-left"></div><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                    </article>
                            <?php if ($i==2 && $total==2 || $i==3): ?>
                                </div>
                            <?php endif ?>
                        <?php endif ?>
                    <?php $i++; endwhile; ?>  
                <div>                               
            </section>                  
        </div>
        <div id="section3" class="inner news section group">
            <section class="col1 col span_1_3">
                <div class="section-wrapper">
                    <div class="green-line"></div>
                    <h2><a href="entertainment">Entertainment</a></h2>
                    <?php
                    $args = array(
                        'posts_per_page' => 3,
                        'category_name' => 'entertainment',
                    );  
             
                    // The Query
                    $the_query = new WP_Query( $args );

                    $i = 1;
                    $total = count($the_query);

                    while ( $the_query->have_posts() ) : $the_query->the_post();
                        if ($i==1):
                    ?>                    
                            <article>
                                <?php if (has_post_thumbnail()): ?>
                                    <?php the_post_thumbnail('thumb-category'); ?>
                                <?php endif ?>
                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                <?php the_excerpt(); ?>
                                <div class="more">
                                    <span class="date"><?php the_time('F j, Y') ?> /</span>
                                    <span class="read-more"><a href="<?php the_permalink(); ?>">Read More &raquo;</a></span>
                                </div>
                            </article>
                        <?php else: ?>
                            <?php if ($i==2): ?>
                                <div class="article-more">
                            <?php endif ?>
                                    <article>
                                        <h3><div class="arrow-left"></div><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                    </article>
                            <?php if ($i==2 && $total==2 || $i==3): ?>
                                </div>
                            <?php endif ?>
                        <?php endif ?>
                    <?php $i++; endwhile; ?>  
                </div>                                  
            </section>
            <section class="col1 col span_1_3">
                <div class="section-wrapper">
                    <div class="green-line"></div>
                    <h2><a href="health">Health</a></h2>
                    <?php
                    $args = array(
                        'posts_per_page' => 3,
                        'category_name' => 'health',
                    );  
             
                    // The Query
                    $the_query = new WP_Query( $args );

                    $i = 1;
                    $total = count($the_query);

                    while ( $the_query->have_posts() ) : $the_query->the_post();
                        if ($i==1):
                    ?>                    
                            <article>
                                <?php if (has_post_thumbnail()): ?>
                                    <?php the_post_thumbnail('thumb-category'); ?>
                                <?php endif ?>
                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                <?php the_excerpt(); ?>
                                <div class="more">
                                    <span class="date"><?php the_time('F j, Y') ?> /</span>
                                    <span class="read-more"><a href="<?php the_permalink(); ?>">Read More &raquo;</a></span>
                                </div>
                            </article>
                        <?php else: ?>
                            <?php if ($i==2): ?>
                                <div class="article-more">
                            <?php endif ?>
                                    <article>
                                        <h3><div class="arrow-left"></div><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                    </article>
                            <?php if ($i==2 && $total==2 || $i==3): ?>
                                </div>
                            <?php endif ?>
                        <?php endif ?>
                    <?php $i++; endwhile; ?>  
                </div>                                  
            </section>
            <section class="col1 col span_1_3">
                <div class="section-wrapper">
                    <div class="green-line"></div>
                    <h2><a href="education">Education</a></h2>
                    <?php
                    $args = array(
                        'posts_per_page' => 3,
                        'category_name' => 'education',
                    );  
             
                    // The Query
                    $the_query = new WP_Query( $args );

                    $i = 1;
                    $total = count($the_query);

                    while ( $the_query->have_posts() ) : $the_query->the_post();
                        if ($i==1):
                    ?>                    
                            <article>
                                <?php if (has_post_thumbnail()): ?>
                                    <?php the_post_thumbnail('thumb-category'); ?>
                                <?php endif ?>
                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                <?php the_excerpt(); ?>
                                <div class="more">
                                    <span class="date"><?php the_time('F j, Y') ?> /</span>
                                    <span class="read-more"><a href="<?php the_permalink(); ?>">Read More &raquo;</a></span>
                                </div>
                            </article>
                        <?php else: ?>
                            <?php if ($i==2): ?>
                                <div class="article-more">
                            <?php endif ?>
                                    <article>
                                        <h3><div class="arrow-left"></div><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                    </article>
                            <?php if ($i==2 && $total==2 || $i==3): ?>
                                </div>
                            <?php endif ?>
                        <?php endif ?>
                    <?php $i++; endwhile; ?>  
                </div>                                  
            </section>              
        </div>
        <div id="section4" class="section group">
            <div id="section4-wrapper" class="inner">
                <section id="about-us" class="col span_1_3">
                    <h2><a href="about-us">About Us</a></h2>
                        <?php
                        if (is_active_sidebar('homepage-about-us')) :
                            dynamic_sidebar('homepage-about-us');
                        endif;
                        ?>
                    <ul id="social-icons">
                        <?php
                        if (is_active_sidebar('homepage-social-buttons2')) :
                            dynamic_sidebar('homepage-social-buttons2');
                        endif;
                        ?>
                    </ul>
                </section>
                <section id="twitter" class="col span_1_3">
                    <div id="twitter-wrapper">
                        <?php
                        if (is_active_sidebar('homepage-twitter')) :
                            dynamic_sidebar('homepage-twitter');
                        endif;
                        ?>
                    </div>
                </section>
                <section id="menu-footer" class="col span_1_3">
                    <?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_class' => 'menu-footer' ) ); ?>
                </section>
            </div>
        </div>
    </div>


<?php get_footer(); ?>    
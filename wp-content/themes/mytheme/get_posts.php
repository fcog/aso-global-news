<?php
/*
Template Name: SYSTEM ONLY - AJAX
*/
?>

<?php
//The Query
$pager = $_REQUEST['pager'];
$paged = (get_query_var(‘paged’)) ? get_query_var(‘paged’) : $pager;
$args=array(
'paged'=>$paged, //Pulls the paged function into the query
'posts_per_page'=>2, //Limits the amount of posts on each page
'category_name'=>'national',
);
query_posts($args);
//The Loop
if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <article class="section group">
        <?php if (has_post_thumbnail()): ?>
            <div class="col span_1_3">
            <?php the_post_thumbnail('thumb-national'); ?>
            </div>
            <aside class="col span_2_3">
        <?php else: ?>
            <aside class="col">
        <?php endif ?>    
            <div class="article-info">
                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                <?php the_excerpt(); ?>
                <div class="more">
                    <span class="date"><?php the_time('F j, Y') ?> /</span>
                    <span class="read-more"><a href="<?php the_permalink(); ?>">Read More &raquo;</a></span>
                </div>
            </div>                      
        </aside>
    </article>
<?php endwhile; endif;
?>
<?php

get_header(); ?>

	<section id="primary" class="content-area inner">

        <div id="content" class="site-content has-sidebar col span_5_6" role="main">

			<header class="archive-header-category">
				<?php if (is_category()): ?>
					<h1 class="archive-title"><?php printf( single_cat_title( '', false ) ); ?></h1>
				<?php else: ?>
					<h1><?php echo ucfirst(get_post_type()) ?></h1>
				<?php endif ?>
			</header><!-- .archive-header -->

			<div id="content-category">
	        <?php
	        $cur_cat = get_cat_ID( single_cat_title("",false) );

	        $paged = (get_query_var('page')) ? get_query_var('page') : 1;

	        $args = array('post_type' => array('post'), 
	        			  'cat' => $cur_cat, 
	        			  'paged' => $paged,
	        			  'posts_per_page' => 15,
	        			  );

	        $posts = new WP_Query( $args );

	        while ($posts->have_posts()) : $posts->the_post();
	        ?>   
	    		<article>
		    		<header class="section group">
			    		<h1 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>						
					</header>
					<div class="section group">
                        <?php if (has_post_thumbnail()): ?>
                            <div class="col span_1_5">
                            <?php the_post_thumbnail(); ?>
                            </div>
                            <aside class="col span_4_5">
                        <?php else: ?>
                            <aside>
                        <?php endif ?>
                        <p class="entry-date">
						<?php _e('Posted') ?> <time><?php the_time('F j, Y') ?></time>
						</p>
						<?php the_excerpt(); ?>
						</aside>
						<div class="read-more"><div class="arrow-left"></div><a href="<?php the_permalink(); ?>">Read more</a></div>
					</div>
				</article>
			<?php endwhile;  ?>	
			</div>	

			<?php wp_pagenavi(); ?>

		</div><!-- #content -->

		<?php if ( is_active_sidebar( 'sidebar-news' ) ) : ?>
			<div id="secondary" class="widget-area col span_1_6" role="complementary">
				<?php dynamic_sidebar( 'sidebar-news' ); ?>
			</div><!-- #secondary -->
		<?php endif; ?>	

	</section><!-- #primary -->

<?php get_footer(); ?>
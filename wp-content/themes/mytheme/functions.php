<?php

function mytheme_setup() {
	register_nav_menu( 'primary', __( 'Navigation Menu', 'mytheme' ) );
	register_nav_menu( 'footer', __( 'Footer Menu', 'mytheme' ) );
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 170, 150, true );
    add_image_size( 'thumb-slider', 454, 264, true ); 
    add_image_size( 'thumb-multimedia', 263, 180, true ); 
    add_image_size( 'thumb-national', 225, 132, true ); 
    add_image_size( 'thumb-category', 310, 143, true ); 
    add_image_size( 'thumb-popular', 100, 85, true ); 
    add_image_size( 'thumb-big', 670, 460, true ); 
}
add_action( 'after_setup_theme', 'mytheme_setup' );

// function add_class_to_menu_items($output, $args) {
// 	if( $args->theme_location == 'primary' )
//   		$output = preg_replace('/class="menu-item/', 'class="col span_1_5 menu-item', $output);
//   return $output;
// }
// add_filter('wp_nav_menu', 'add_class_to_menu_items', 10, 2);

function add_div_to_menu_items($output, $args) {
	if( $args->theme_location == 'footer' )
  		$output = preg_replace('/<a href/', '<div class="arrow-left"></div><a href', $output);
  return $output;
}
add_filter('wp_nav_menu', 'add_div_to_menu_items', 10, 2);

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function mytheme_widgets_init() {

/************* HEADER *********************/
    register_sidebar( array(
      'id'            => 'homepage-social-buttons',
      'name'          => __( 'Homepage - Social buttons', 'mytheme' ),
      'before_widget'  => '',                  
    ) );

    register_sidebar( array(
      'id'            => 'homepage-social-buttons2',
      'name'          => __( 'Homepage - Social buttons Bottom', 'mytheme' ),
      'before_widget'  => '',                  
    ) );    

    register_sidebar( array(
      'id'            => 'homepage-popular',
      'name'          => __( 'Homepage - Popular', 'mytheme' ),
      'before_widget'  => '',                  
    ) );     

    register_sidebar( array(
      'id'            => 'homepage-twitter',
      'name'          => __( 'Homepage - Twitter', 'mytheme' ),
      'before_widget'  => '',                  
    ) );
 
    register_sidebar( array(
      'id'            => 'homepage-about-us',
      'name'          => __( 'Homepage - About Us', 'mytheme' ),
      'before_widget'  => '',                  
    ) );      

    register_sidebar( array(
      'id'            => 'footer-copyright',
      'name'          => __( 'Footer - Copyright', 'mytheme' ),
      'before_widget'  => '',                  
    ) );       

    register_sidebar( array(
      'id'            => 'sidebar-blog',
      'name'          => __( 'Sidebar - Blog', 'mytheme' ),
      'before_widget'  => '',                  
    ) );  
}
add_action( 'widgets_init', 'mytheme_widgets_init' );

/************* CUSTOM WIDGETS *****************/


/************* SOCIAL LINKS *****************/

class SocialLinkWidget extends WP_Widget {
         public function __construct() {
               parent::WP_Widget(false,'Social Link','description=Social Link');
        }

        public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, array( 'class' => '', 'link' => '' ) );
			    $class = $instance['class'];
			    $link = $instance['link'];
			?>
			  <p><label for="<?php echo $this->get_field_id('class'); ?>">CSS Class: </label><input class="widefat" id="<?php echo $this->get_field_id('class'); ?>" name="<?php echo $this->get_field_name('class'); ?>" type="text" value="<?php echo attribute_escape($class); ?>"/></p>

			  <p><label for="<?php echo $this->get_field_id('link'); ?>">Link: </label><input class="widefat" id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>" type="text" value="<?php echo attribute_escape($link); ?>" /></p>
			<?php
        }

        public function update( $new_instance, $old_instance ) {
		    $instance = $old_instance;
		    $instance['class'] = $new_instance['class'];
		    $instance['link'] = $new_instance['link'];
		    return $instance;
        }

        public function widget( $args, $instance ) {
		    extract($args, EXTR_SKIP);
		 
		    echo $before_widget;
		    $class = empty($instance['class']) ? '' : apply_filters('widget_title', $instance['class']);
		    $link = empty($instance['link']) ? '' : apply_filters('widget_title', $instance['link']);

		    if (!empty($link))
		      echo "<li class='".$class."'><a href='".$link."' target='_blank'></a></li>";

		    echo $after_widget;
        }

}
register_widget( 'SocialLinkWidget' );

/************* HELPER FUNCTIONS *****************/

//Remove menu div container
function prefix_nav_menu_args($args = ''){
    $args['container'] = false;
    return $args;
}
add_filter('wp_nav_menu_args', 'prefix_nav_menu_args');

// function html_widget_title( $title ) {
// 	//HTML tag opening/closing brackets
// 	$title = str_replace( '[', '<', $title );
// 	$title = str_replace( ']', '/>', $title );

// 	return $title;
// }
// add_filter( 'widget_title', 'html_widget_title' );

// function html_widget_text( $text ) {
// 	//HTML tag opening/closing brackets
// 	$text = str_replace( '[', '<', $text );
// 	$text = str_replace( ']', '/>', $text );

// 	return $text;
// }
// add_filter( 'widget_text', 'html_widget_text' );


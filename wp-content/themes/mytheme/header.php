<!DOCTYPE html>
<html lang="en">
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie-7"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie-8"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <!-- Responsive and mobile friendly stuff -->
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--jQuery-->
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.8.2.min.js"></script>

    <!--Styles-->
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/style.css" />

    <!--prefix free-->
    <script src="<?php echo get_template_directory_uri(); ?>/js/prefixfree.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/modernizr-2.5.3-min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/selectivizr-min.js"></script>    

    <!--Only Mac - Safari Class-->
    <script type="text/javascript">
    jQuery(function(){
        // console.log(navigator.userAgent);
        /* Adjustments for Safari on Mac */
        if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Mac') != -1) {
            jQuery('html').addClass('mac'); // provide a class for the safari-mac specific css to filter with
        }
    });
    </script>

    <!--Selectivizr-->
    <!--[if (gte IE 6)&(lte IE 8)]>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/selectivizr-min.js"></script>
    <![endif]-->

    <!--[if IE]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--[if gte IE 9]>
        <style type="text/css">
         .gradient {
            filter: none;
        }
     </style>
    <![endif]-->

    <?php  if (is_home()): ?>
    <link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/css/flexslider.css" />   
    <?php endif ?>    

	<title><?php echo get_bloginfo("name"); ?></title>
<?php wp_head(); ?>
</head>
<body>

<div id="wrapper">
    <header id="header">
        <div id="row1-wrapper" class="section group">
            <div class="row1 inner">
                <div id="header-date" class="col span_3_4">
                    <span class="icon"></span>
                    <span class="text"><?php echo date("l, F j, Y", time()) ?></span>
                </div>
                <ul id="social-icons" class="col span_1_4">
                    <?php
                    if (is_active_sidebar('homepage-social-buttons')) :
                        dynamic_sidebar('homepage-social-buttons');
                    endif;
                    ?>
                </ul>
            </div>  
        </div>
        <div id="row2-wrapper" class="section group">
            <div id="row2-bkgd-wrapper">
                <div class="row2 inner">
                    <h1 id="header-logo" class="col span_3_4">
                        <a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png"></a>
                    </h1>
                    <div id="search-form" class="col span_1_4">
                        <?php get_search_form(); ?>
                    </div>
                </div>
            </div>
        </div>          
        <nav class="section group">
            <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'inner' ) ); ?>
        </nav>
    </header>    
    <footer id="footer" class="section group">
        <div class="inner">
            <?php
            if (is_active_sidebar('footer-copyright')) :
                dynamic_sidebar('footer-copyright');
            endif;
            ?>          
        </div>
    </footer>
</div>
<?php wp_footer() ?>
</body>
<?php  if (is_home()): ?>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider-min.js"></script>
<script type="text/javascript" charset="utf-8">
    var permission_prev = false;
    var permission_next = true;

    jQuery(window).load(function() {
        jQuery('#slideshow').flexslider({
            animation: "fade",
            direction: "horizontal",
            slideshowSpeed: 7000,
            animationSpeed: 600,
            directionNav: true,
            initDelay: 0,
            prevText: "",           
            nextText: "",
        });
    });

    function disable(){
        var pager = parseInt($('#pager').html());
        var total = parseInt($('#total-posts').html());
        var pages = Math.ceil(total/2);

        if (pager == 1){ 
            $('#slider-controls .prev').css('opacity','0.5').css('cursor','auto');
            permission_prev = false;
        }
        else{
            $('#slider-controls .prev').css('opacity','1').css('cursor','pointer');
            permission_prev = true;            
        }

        if (pager == pages){ 
            $('#slider-controls .next').css('opacity','0.5').css('cursor','auto');
            permission_next = false;
        }
        else{
            $('#slider-controls .next').css('opacity','1').css('cursor','pointer');
            permission_next = true;            
        }
    }

    $(document).ready(function () {
        $('#global-wrapper').css('height',$('#slideshow').height()+50);

        disable();  

        $('#slider-controls .next').click(function(){
            if (permission_next){
                $('#national').css('opacity','0.5');
                $('#national').append('<img src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.gif" id="loader">');
                var pager = parseInt($('#pager').html())+1;
                $('#pager').html(pager);
                $.get("system-use-only", { pager: pager }).done(function( data ) {
                   $("#national-article-wrapper").html( data );
                    $('#national').css('opacity','1');
                    $('#loader').remove();
                    disable();           
                });
            }
        });
        $('#slider-controls .prev').click(function(){
            if (permission_prev){
                $('#national').css('opacity','0.5');
                $('#national').append('<img src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.gif" id="loader">');
                var pager = parseInt($('#pager').html())-1;
                $('#pager').html(pager);
                $.get("system-use-only", { pager: pager }).done(function( data ) {
                   $("#national-article-wrapper").html( data );
                    $('#national').css('opacity','1');
                    $('#loader').remove();
                    disable();            
                });
            }
        });        
    });    
</script>    
<?php endif ?>
</html>